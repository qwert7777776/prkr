from typing import List
from scipy.optimize import minimize
import numpy as np

num_rectangles = 10


def total_intersection_area(params):
    rects = params.reshape(-1, 4)
    total_area = 0.0
    for i in range(num_rectangles):
        for j in range(i + 1, num_rectangles):
            area = intersection_area(rects[i], rects[j])
            total_area += area
    return total_area


def intersection_area(rect1, rect2):
    x_left = max(rect1[0], rect2[0])
    y_bottom = max(rect1[1], rect2[1])
    x_right = min(rect1[2], rect2[2])
    y_top = min(rect1[3], rect2[3])
    if x_right < x_left or y_top < y_bottom:
        return 0.0
    else:
        return (x_right - x_left) * (y_top - y_bottom)


def minimize_intersection(rects: List[List[float]]) -> List[List[float]]:
    """
    Функция принимает список координат прямоугольников в формате
    (x_min, y_min, x_max, y_max), и возвращает новые координаты,
    минимизирующие общую площадь пересечения между прямоугольниками.
    """

    def objective(x: List[float], *args) -> float:
        """
        Функция, которую мы хотим минимизировать. Она принимает
        один аргумент x, который является списком всех координат
        прямоугольников в формате (x_min, y_min, x_max, y_max).
        """
        num_rects = len(rects)
        new_rects = np.reshape(x, (num_rects, 4))
        return total_intersection_area(new_rects)

    # Исходные координаты прямоугольников в виде одномерного массива
    x0 = np.array(rects).flatten()

    # Определяем ограничения на значения координат
    bounds = [(None, None)] * len(x0)
    for i in range(len(x0)):
        if i % 4 == 0 or i % 4 == 2:
            # x_min и x_max не могут быть меньше нуля
            bounds[i] = (0, None)
        if i % 4 == 1 or i % 4 == 3:
            # y_min и y_max не могут быть меньше нуля
            bounds[i] = (0, None)

    # Минимизируем функцию objective с помощью метода SLSQP
    res = minimize(objective, x0, args=(), method='SLSQP', bounds=bounds)

    # Новые координаты прямоугольников в виде двумерного массива
    new_rects = np.reshape(res.x, (len(rects), 4))
    return new_rects.tolist()


import random


def test_minimize_intersection():
    # Создаем список случайных прямоугольников
    rectangles = []
    for i in range(num_rectangles):
        x_min = np.random.uniform(0, 10)
        y_min = np.random.uniform(0, 10)
        x_max = np.random.uniform(x_min, 10)
        y_max = np.random.uniform(y_min, 10)
        rectangles.append([x_min, y_min, x_max, y_max])

    # Минимизируем общую площадь пересечения
    new_rects = minimize_intersection(rectangles)

    # Проверяем, что все координаты новых прямоугольников неотрицательны
    for rect in new_rects:
        assert rect[0] >= 0
        assert rect[1] >= 0
        assert rect[2] >= 0
        assert rect[3] >= 0

    # Проверяем, что общая площадь пересечения меньше, чем до минимизации
    assert total_intersection_area(np.array(new_rects)) < total_intersection_area(np.array(rectangles))


test_minimize_intersection()